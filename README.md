# prep_data_chiros

**NB : this pipeline is set to work with data over continental France, most of
the features come from IGN public data and transformations are done with
EPSG:2154 (but rendered in 4326 in the end). It is probably usable on other
territories but it would need some adjustments.**


R + Python pipeline to prepare data in order to study the impact of environmental
and structural parameters on bat activity.

The entry point in the pipeline is PCIA_Pipeline_GIS.R where setwd() is to be
set at the root of your project folder which must contain a data folder with
following structure: 
```
data
  └─ dependances_fixes
  │     └─ SysGrid_500m_de_cote.csv (from Create_empty_grid_V2.R)
  │     └─ SysGrid_2000m_de_cote.csv (from Create_empty_grid_V2_2000.R)
  │     └─ a ROI.gpkg file with EPSG:4326
  │ 
  └─ observations
  │     └─ DataVigieChiro_04.csv
  │     └─ DataVigieChiro_83.csv
  │     └─ ...
  │
  └─ prep_data (folder names can be adjusted in PCIA_Pipeline_GIS.R)
        └─ ALAN
        └─ BDALTI
        └─ BD_TOPAGE_2023-shp
        └─ CLC
        └─ OCS_OSO
        └─ ROUTE500_...
        └─ VCF
```

## Requirements

In order to work the scripts in this repo need some python modules to be
installed, if you don’t have the required modules (and versions) or don’t want
to mess your environment, I recommend to execute them in a separate
environment.

```bash
# In the scripts folder
# first install virtualenv if you don’t have it already:
pip install virtualenv

# then create a new environment, let’s call it "vigie":
virtualenv vigie

# we can now activate it and install the modules
source vigie/bin/activate
pip install -r requirements.txt

# if you want to deactivate the environment, just run:
deactivate

# the meteo part of this pipeline depends on python, so before you execute the
# PCIA_Pipeline_GIS.R script (directly in the terminal or in RStudio terminal console), execute :
source vigie/bin/activate
```
## Data Preparation 

### ALAN (NASA’s VIIRS data)

Downloaded with download_virs.R (based on the R package [blackmaRble](https://github.com/giacfalk/blackmaRble)).
Needs ROI.gpkg. 
Calls on scripts/_vars.R for earthdata username and password.
Produces a monthly image over the desired period (median).

### BDALTI

Elevation over France with spatial resolution of 25m
.asc files from IGN’s [BD ALTI](https://geoservices.ign.fr/bdalti), just
extract the .asc files in the BDALTI folder.

### BD_TOPAGE_2023-shp

Downloadable [there](https://www.sandre.eaufrance.fr/atlas/srv/fre/catalog.search#/metadata/7fa4c224-fe38-4e2c-846d-dcc2fa7ef73e),
might need to update PCIA_Pipeline_GIS with the correct name as it is updated
regularly.

### CLC

Corine Land Cover raster data. Downloaded from [land.copernicus.eu](https://land.copernicus.eu/en/products/corine-land-cover).<br>
Multiple years (only 2012 and 2018 are useful for vigie-chiro data), 100m
spatial resolution.

Those rasters need treatment, the classes are coded 1-48 for space management in
the original files, reclassify_CLC_term.R reclassifies them with the 3 digits
classes (needs classes_clc.csv)
with classes_clc.csv in the CLC folder run :

```bash
Rscript reclassify_CLC_term.R U2018_CLC2018_V2020_20u1.tif
# outputs : U2018_CLC2018_V2020_20u1R.tif (notice the "R" at the end)
```
Has to be ran for every CLC raster in the CLC folder and original CLC raster
needs to be moved/deleted after the reclassification.


### OCS OSO

[Land Usage by CES](https://www.theia-land.fr/product/carte-doccupation-des-sols-de-la-france-metropolitaine/)
Has finer spatial (10m) and temporal(yearly) resolution than CLC. 
We chose to not use 2016 and 2017 data that have different classes numbers.

Name should be in the format "OCS_YEAR.tif" so the script can choose the closest
to the data by year.

### Route 500

Ways and transport (roads, train) from IGN [roads database](https://geoservices.ign.fr/route500).
Raw extraction of the downloaded zip file.

(nb: might need to switch to IGN’s BD CARTO at some point)


### MODIS VCF Percent Tree Cover

As described [here](https://lpdaac.usgs.gov/products/mod44bv061/), is a yearly
product by NASA satellite MODIS representing vegetation cover.

Downloaded with download_vcf.R script. (needs to setup scripts/_vars.R with
username and password variables from your earthdata account)

## Scripts

Most of the scripts can be used alone if you only need part of the data (or
update your data without executing everything).

### get_meteo.py

**NB: I acquired an api key from open-meteo since I first scripted this pipeline,
if you have one, it should be written in scripts/_vars.py as a variable named
meteo_api_key, else uncomment the following line in get_meteo.py :**
```python
# data_weathered = fun.split_get(data, 500, m=monthly)
```
**and comment this one :**
```python
data_weathered = fun.get_open_weather_api_key(data, monthly=monthly)
```

Interrogates [open-meteo.com](https://open-meteo.com/en/docs/historical-weather-api)
historical api, from a csv with X and Y columns + Nuit (date) or unique date
passed as argument.

Respects open-meteo’s limitations(600 requests per minute, 5000 per hour, 10000
per day) by waiting between attempts.

The data is to be used to train a Random Forest Regressor on Bat activity,
night values only are kept.

Returns a csv file in the same folder as the input csv
The data is to be used to train a Random Forest Regressor on Bat activity,
night values only are kept.

The new .csv file is saved in the same folder as the input .csv with name
"csv_original_name_meteo.csv".

This new .csv has added columns:
- Spmean_temp : mean temperature at 2m 
- Spmin_temp : minimal temperature
- Spmax_temp : maximal temperature
- Spmean_wind : mean wind at 10m
- Spmin_wind : minimal wind speed
- Spmax_wind : maximal wind speed
- Sptotal_precipitations : total precipitations during the night

In training mode these values are at the night scale, in predict mode, they are
the median of the month you choose with the --date option

You can run this script from a unix terminal (haven’t tested it on Win) with
options :
- --mode : "train" or "predict"
- --date : only if mode = predict, yyyy-mm-dd format
- --file : absolute path to .csv file, containing "X" (longitude) and "Y"
(latitude) columns in predict mode and also a "Nuit" column (date : yyyy-mm-dd)
in train mode

### group_obs.py

Combines training/predict variables in one .csv file, callable from the
terminal, else called from PCIA_Pipeline_GIS.R as a part of the data
preparation.

If called with --mode predict, needs --date to be set (yyyy-mm-dd format), as
the predictions are made at the month scale.

Else, if called with --mode train, you should specify which species you want to
train your data on with the --sp option, that takes 6 characters string
shortnames for bats as "Barbar, Pippip..."

It takes one more __required__  option : --loc that should point to your "data"
folder.

Output files are either :
- vars_obs_{species}.csv
- vars_predict_{year}_{month}.csv

### reclassify_CLC_term.R
This script is callable directly from the terminal inside the CLC folder with
classes_clc.csv file present. It takes the name of the CLC file you want to
reclassify as unique argument.

It outputs a raster with the same name + 'R' at the end and pixel values on
three digits corresponding to Corine Land Cover classes..
