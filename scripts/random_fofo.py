from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import geopandas as gpd
import rasterio as rio
from sklearn.model_selection import train_test_split, KFold, RepeatedKFold, GroupKFold, GridSearchCV
from sklearn.metrics import mean_squared_error as MSE
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.metrics import accuracy_score as OA
from sklearn.metrics import r2_score as R2
from sklearn.ensemble import RandomForestRegressor as RF
from BorutaShap import BorutaShap
import statsmodels.api as sm
from joblib import parallel_backend
from joblib import dump, load
import os
from osgeo import gdal
import random
# matplotlib.use('AGG')
# à adapter à l’usage de ce script :
matplotlib.use('module://matplotlib-backend-kitty')

data_folder = "/home/bbk9/Documents/asellia/Barba_2024/data/dependances_fixes"

# Choix de l’espèce
espece = "Barbar"

# Préparation du jeu d’entrainement
train_data_file = os.path.join(data_folder, f"vars_obs_{espece}.csv")
train_data = pd.read_csv(train_data_file)
train_data = train_data.rename(
    columns={'X': 'SpX', 'Y': 'SpY', 'Yday': 'SpYday'})
train_data["nb_contacts_log"] = np.log10(train_data["nb_contacts"] + 1)

valid_data_file = os.path.join(data_folder, f"vars_valid_{espece}.csv")
valid_data = pd.read_csv(valid_data_file)
valid_data = valid_data.rename(
    columns={'X': 'SpX', 'Y': 'SpY', 'Yday': 'SpYday'})
valid_data["nb_contacts_log"] = np.log10(valid_data["nb_contacts"] + 1)

len(train_data)

len(valid_data)


def remove_spatial_correlation(dataframe):
    nombre_codes = dataframe.code.value_counts()
    codes_nombreux = nombre_codes[nombre_codes > 1]
    codes = codes_nombreux.index
    data_ok = dataframe[~dataframe.code.isin(codes)]
    data_not_ok = dataframe[dataframe.code.isin(codes)]
    for code in codes:
        rows_to_choose_from = data_not_ok[data_not_ok["code"] == code]
        nb_rows_to_choose_from = len(rows_to_choose_from)
        good_row_number = random.randrange(nb_rows_to_choose_from)
        # nb_contacts_max = rows_to_choose_from.loc[:, "nb_contacts"].max()
        # good_row = rows_to_choose_from[rows_to_choose_from["nb_contacts"]
        # == nb_contacts_max]
        data_ok = data_ok.append(
            rows_to_choose_from.iloc[good_row_number], ignore_index=True)
    return (data_ok)


train_data_positif = train_data[train_data["nb_contacts"] != 0]
percentile = np.percentile(train_data_positif.nb_contacts_log, 98)
train_data = train_data[train_data["nb_contacts_log"] <= percentile]

train_data = remove_spatial_correlation(train_data)


valid_data_positif = valid_data[valid_data["nb_contacts"] != 0]
percentile_valid = np.percentile(valid_data_positif.nb_contacts_log, 98)
valid_data = valid_data[valid_data["nb_contacts_log"] <= percentile_valid]


OUTPUT_FEATURE = ["nb_contacts_log"]
X = train_data.filter(regex="Sp*")
Y = train_data.loc[:, OUTPUT_FEATURE].values.ravel()
code = train_data["code"].to_numpy()
X_valid = valid_data.filter(regex="Sp*")
Y_valid = valid_data.loc[:, OUTPUT_FEATURE].values.ravel()
missing_vars = list(set(X.columns) - set(X_valid.columns))
X_valid = X_valid.reindex(columns=[*X_valid.columns.tolist() + missing_vars])
X_valid = X_valid.fillna(0)
X_valid = X_valid.loc[:, X.columns]

param_grid = {
    'n_estimators': [200, 400, 600, 800],
    'max_features': ['sqrt', 'log2', 0.3, 0.5, 0.7],
}
X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
                                                    train_size=0.7,
                                                    random_state=12)
model = RF(random_state=0)
grid = GridSearchCV(model, param_grid=param_grid, n_jobs=-1,
                    scoring='r2',
                    refit=True,
                    cv=KFold(n_splits=5,
                             shuffle=True,
                             random_state=18))
grid.fit(X_train, Y_train)

score = grid.best_estimator_.score(X_test, Y_test)
res = [[param["max_features"], param["n_estimators"], err]
       for param, err in zip(grid.cv_results_["params"],
                             grid.cv_results_["mean_test_score"])]
fig, ax = plt.subplots(nrows=1, ncols=1)
im = ax.imshow(grid.cv_results_["mean_test_score"].reshape(5, 4))
ax.set_xticks(range(4))
ax.set_xticklabels(param_grid["n_estimators"])
ax.set_yticks(range(5))
ax.set_yticklabels(param_grid["max_features"])
cb = fig.colorbar(im, ax=ax)
ax.set_title(
    f"Best parameters for species {espece} {grid.best_params_} \n Train score {grid.best_score_:.2f} \n Test score: {score:.2f}")
plt.show()

grid.best_params_  # nyclei max_features 0.5 n_est 400

max_features = grid.best_params_["max_features"]
n_estimators = grid.best_params_["n_estimators"]


# test avec best params
r2 = []
oob_score = []
for i in range(30):
    for ii in range(30):
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
                                                            train_size=0.7,
                                                            random_state=i)
        model = RF(oob_score=True,
                   bootstrap=True,
                   max_features=max_features,
                   n_estimators=n_estimators,
                   random_state=ii,
                   n_jobs=-1)
        model.fit(X_train, Y_train)
        prec_test = model.score(X_test, Y_test)
        predictions = model.predict(X_test)
        Y_predict_nb = np.round((10 ** predictions) - 1, 0)
        Y_test_nb = np.round((10 ** Y_test) - 1, 0)
        r2_score = R2(Y_test_nb, Y_predict_nb)
        r2.append(r2_score)
        oob_score.append(model.oob_score_)
        # predictions = model.predict(X_test)
# r2_score = R2(Y_test, predictions)
# scores.append(r2_score)
# print(f"R2 : {r2_score}")
# print(f"MSE : {MSE(Y_test, predictions)}")
# print(f"MAE : {MAE(Y_test, predictions)}")

oob_score.index(max(oob_score))


plt.plot(r2)
plt.show()

plt.plot(oob_score)
plt.show()
# Récupération et visualisation des variables principales


X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
                                                    train_size=0.7,
                                                    random_state=12)

model = RF(oob_score=True,
           bootstrap=True,
           max_features=max_features,
           n_estimators=n_estimators,
           random_state=i,
           n_jobs=-1)
model.fit(X, Y)
predictions = model.predict(X)

Y_orig = list(train_data.loc[:, "nb_contacts"].values.ravel())
Y_predict = np.round((10 ** predictions) - 1, 0)
Y_predict_nb_contacts = list(Y_predict)
R2(Y_orig, Y_predict_nb_contacts)
cols = {'y_orig': Y_orig, 'y_predict': Y_predict_nb_contacts}
df_compare = pd.DataFrame(cols)
df_compare

stats.describe(Y_predict)


Y_predict_valid = model.predict(X_valid)

Y_predict_valid_nb_contacts = list(np.round((10 ** Y_predict_valid) - 1, 0))
Y_valid_nb_contacts = list(valid_data.loc[:, "nb_contacts"].values.ravel())

cols_valid = {'y_orig': Y_valid_nb_contacts,
              'y_predict': Y_predict_valid_nb_contacts}
df_compare_valid = pd.DataFrame(cols_valid)

df_compare_valid["diff"] = abs(df_compare_valid["y_predict"] -
                               df_compare_valid["y_orig"])

df_compare_valid["y_predict"].describe()
df_compare_valid["y_orig"].describe()

R2(Y_predict_valid_nb_contacts, Y_valid_nb_contacts)


# r2_score = R2(Y_valid, predictions)

model.oob_score_

max(scores)

prec_valid

len(X_valid)

# Récupération des noms de variables
col_train = X.columns


importances = model.feature_importances_
important_features_dict = {}
for idx, val in enumerate(importances):
    important_features_dict[idx] = val
important_features_list = sorted(important_features_dict,
                                 key=important_features_dict.get,
                                 reverse=True)
feature_names_ordered = col_train[important_features_list]

# print(f'50 most important features: {important_features_list[:50]}')

valeurs_ordonnees = sorted(
    important_features_dict.values(), key=lambda x: x, reverse=True)
index_importances_05 = 0
somme = 0
for elem in valeurs_ordonnees:
    while somme <= 0.5:
        somme += elem
        index_importances_05 += 1

std = np.std([tree.feature_importances_ for tree in model.estimators_], axis=0)
std_ordonnee = std[important_features_list]
forest_importances = pd.Series(valeurs_ordonnees[:index_importances_05],
                               index=feature_names_ordered[:index_importances_05])
fig, ax = plt.subplots()
forest_importances.plot.bar(yerr=std_ordonnee[:index_importances_05], ax=ax)
ax.set_title("Feature importances explaining 50% of the training")
ax.set_ylabel("Mean decrease in impurity")
fig.tight_layout()
fig.savefig("test.png")
plt.show()


# Prédictions sur la grille 500m paca
for mois in range(4, 12):
    mois = str(mois).zfill(2)
    predict_data_file = os.path.join(
        data_folder, f"vars_predict_2023-{mois}.csv")
    predict_data = pd.read_csv(predict_data_file)
    predict_map_points = predict_data[["X", "Y"]]
    predict_data = predict_data.rename(
        columns={'X': 'SpX', 'Y': 'SpY', 'Yday': 'SpYday'})
    predict_data = predict_data.filter(regex="Sp*")
    predict_data = predict_data[col_train]
    predict_map_points = gpd.GeoDataFrame(
        predict_map_points,
        geometry=gpd.points_from_xy(predict_map_points.X,
                                    predict_map_points.Y),
        crs="EPSG:4326")
    predict_map_points = predict_map_points.drop(columns=["X", "Y"])
    # prédictions
    predict_map = model.predict(predict_data)
    predict_nb_contacts = np.round((10 ** predict_map) - 1, 0)
    predict_map_points["value"] = predict_nb_contacts
    # predict_map_points.value = predict_map_points.value.astype('uint16')
    predict_map_points = predict_map_points.to_crs(2154)
    predict_map_points.to_file(f"{espece}_test_2023-{mois}.gpkg",
                               overwrite=True)


predict_map_points.total_bounds

nodata_value = 65535
vector_name = "test.shp"
raster_name = "test.tif"
pixel_size = 500

points = gdal.OpenEx(vector_name)

type(points)

x_min, y_min, x_max, y_max = predict_map_points.total_bounds
x_min = x_min - 250
y_min = y_min - 250
x_max = x_max + 250
y_max = y_max + 250

points = gdal.OpenEx(vector_name)
gdal.Rasterize(raster_name, points, format='GTiff',
               outputType=gdal.GDT_UInt16,
               # attribute='value',
               burnValues=1,
               # bands=[0],
               outputSRS="EPSG:2154",
               noData=nodata_value,
               outputBounds=[x_min, y_min, x_max, y_max],
               xRes=pixel_size,
               yRes=pixel_size,
               )
