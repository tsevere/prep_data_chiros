import pandas as pd
import glob
import os
import numpy as np
import datetime
import ephem
import sys
import argparse

main_parser = argparse.ArgumentParser()
main_parser.add_argument("--mode",
                         help=""""Indicate if you want to get data for training
                            (based on observation data) or predicting""",
                         type=str, required=True,
                         choices=["train", "predict", "valid"])
main_parser.add_argument("--loc",
                         help="""Indicate the folder where the data is
                         located (absolute path)""", type=str, required=True)
main_parser.add_argument("--sp",
                         help="""If mode=train, for which bat species do you
                         want to train ? 6 character species code Barbar,
                         Pippip...""", type=str, required=False)
main_parser.add_argument("--date", help="""Set date for prediction (format:
                                        YYYY-mm-dd)""",
                         type=str, required=False)

main_args, _ = main_parser.parse_known_args()


data_folder = main_args.loc
vars_folder = os.path.join(data_folder, "dependances_fixes")

if main_args.mode == "train":
    mode = "train"
elif main_args.mode == "valid":
    mode = "valid"
elif main_args.mode == "predict":
    mode = "SysGrid_500m"
    date_pred = main_args.date[0:7]

print(mode)
# Fichiers

alan = glob.glob(vars_folder + "/*" + mode + "*" + "ALAN" + "*")
alti = glob.glob(vars_folder + "/*" + mode + "*" + "Alti" + "*")
carthage = glob.glob(vars_folder + "/*" + mode + "*" + "Carthage" + "*")
clc = glob.glob(vars_folder + "/*" + mode + "*" + "CLC" + "*")
ocs = glob.glob(vars_folder + "/*" + mode + "*" + "OCS" + "*")
meteo = glob.glob(vars_folder + "/*" + mode + "*" + "meteo" + "*")
transports = glob.glob(vars_folder + "/*" + mode + "*" + "Transports" + "*")
vcf = glob.glob(vars_folder + "/*" + mode + "*" + "VCF" + "*")

alan = pd.read_csv(alan[0])
alti = pd.read_csv(alti[0])
carthage = pd.read_csv(carthage[0])
clc = pd.read_csv(clc[0])
ocs = pd.read_csv(ocs[0])
meteo = pd.read_csv(meteo[0], index_col=0)
transports = pd.read_csv(transports[0])
vcf = pd.read_csv(vcf[0])


alti = alti.drop_duplicates()
carthage = carthage.drop_duplicates()
transports = transports.drop_duplicates()
vars = alan.merge(alti, on=["X", "Y"], how="left")
vars = vars.merge(carthage, on=["X", "Y"], how="left")
vars = vars.merge(transports, on=["X", "Y"], how="left")
if main_args.mode == "predict":
    vars["Nuit"] = main_args.date
vars = vars.merge(clc, on=["X", "Y", "Nuit"], how="left")
vars = vars.merge(ocs, on=["X", "Y", "Nuit"], how="left")
vars = vars.merge(vcf, on=["X", "Y", "Nuit"], how="left")
vars = vars.merge(
    meteo, on=["X", "Y", "Nuit"], how="left")


# if mode = train
if mode == "train":
    obs_folder = os.path.join(data_folder, "observations")
    all_obs = glob.glob(os.path.join(obs_folder, "*.csv"))

    obs = []
    for file in all_obs:
        print(file)
        df = pd.read_csv(file, index_col=None, header=0,
                         encoding="Latin1", sep=";")
        obs.append(df)
    observations = pd.concat(obs, axis=0, ignore_index=True)

    espece_choisie = main_args.sp

    observations_sp = observations.query(f'espece == "{espece_choisie}"')

    colonnes = ["longitude", "latitude", "Nuit", "nb_contacts"]
    observations_sp = observations_sp[colonnes]

    vars = vars.merge(observations_sp, how="left", left_on=["X", "Y", "Nuit"],
                      right_on=["longitude", "latitude", "Nuit"])

    loc_code = pd.read_csv(f"{vars_folder}/loc_train.csv", usecols=["X", "Y",
                                                                    "Nuit",
                                                                    "code"])
    vars = vars.merge(loc_code, how="left", on=["X", "Y", "Nuit"])
    vars = vars.drop(["longitude", "latitude"], axis=1)
elif mode == "valid":
    valid_file_name = "scanR.csv"
    file_loc = os.path.join(data_folder, "observations", valid_file_name)
    observations = pd.read_csv(file_loc)

    espece_choisie = main_args.sp

    observations_sp = observations.query(f'ID == "{espece_choisie}"')

    colonnes = ["longitude", "latitude", "Nuit", "nombre"]
    observations_sp = observations_sp[colonnes]
    observations_sp = observations_sp.rename(columns = {"nombre": "nb_contacts"})
    observations_sp["Nuit"] = observations_sp["Nuit"].str.replace("/", "-")

    vars = vars.merge(observations_sp, how="left", left_on=["X", "Y", "Nuit"],
                      right_on=["longitude", "latitude", "Nuit"])

    loc_code = pd.read_csv(f"{vars_folder}/loc_valid.csv", usecols=["X", "Y",
                                                                    "Nuit",
                                                                    "code"])
    vars = vars.merge(loc_code, how="left", on=["X", "Y", "Nuit"])
    #vars = vars.drop(["longitude", "latitude"], axis=1)
# mettre jointure pour sysgrid

vars.fillna(0, inplace=True)

date = pd.to_datetime(vars['Nuit'])
yearday = date.dt.strftime('%j')
vars['Yday'] = yearday
vars["SpCDate"] = np.cos(yearday.astype(str).astype(int) / 365 * 2 * np.pi)
vars["SpSDate"] = np.sin(yearday.astype(str).astype(int) / 365 * 2 * np.pi)

year = date.dt.strftime('%Y')
vars['SpYear'] = year

datemoon = date.apply(ephem.Date)
moon = datemoon.apply(ephem.Moon)
phase = moon.apply(lambda x: x.moon_phase)
vars["SpMoon"] = phase


if mode == "train":
    vars.to_csv(os.path.join(
        vars_folder, f"vars_obs_{espece_choisie}.csv"), sep=",", index=False)
elif mode == "valid":
    vars.to_csv(os.path.join(
        vars_folder, f"vars_valid_{espece_choisie}.csv"), sep=",", index=False)
else:
    vars.to_csv(os.path.join(
        vars_folder, f"vars_predict_{date_pred}.csv"), sep=",", index=False)
